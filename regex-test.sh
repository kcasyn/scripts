#!/bin/bash

# Simple script to perform bash regex on a string

# This is the string you want to match
STRING=""

# This is your bash regex
REGEX=""

# perform the regex
if [[ $STRING =~ $REGEX ]]
then
    # if the regex works, print True
    echo True
else
    # if the regex doesn't match, print False and both the string and regex as bash sees them
    echo False
    echo $STRING
    echo $REGEX
fi
