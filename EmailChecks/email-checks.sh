#!/bin/bash

### INFO
# Created by kcasyn

### Colors

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

### Variables

# Domain to check
DOMAIN=$1
# Email header file of an email from the domain to be checked
HEADER_FILE=$2

# If a header file is supplied parse for DKIM selectors
if [[ ! -z $HEADER_FILE ]]
then
    DKIM_SELECTORS=($(awk '/s=.*/{ print $2 }' $HEADER_FILE | sed 's/s=//g' | sed 's/;//g' | tr '\n' ' '))
fi

echo -e "[${YELLOW}+${RESET}] Checking for MX records"

# Check if MX records exist
if [[ ! -z $(dig mx $DOMAIN +short) ]]
then
    echo -e "[${GREEN}!${RESET}] MX records found"
    echo -e "[${YELLOW}+${RESET}] Checking for SPF"

    # Check if SPF records exist
    if [[ ! -z $(dig txt $DOMAIN +short | grep spf) ]]
    then
	dig txt $DOMAIN +short | grep spf
    else
	echo -e "[${RED}!${RESET}] No SPF records found"
    fi

    echo -e "[${YELLOW}+${RESET}] Checking for DMARC"

    # Check if DMARC records exist
    if [[ ! -z $(dig txt _dmarc.$DOMAIN +short) ]]
    then
	dig txt _dmarc.$DOMAIN +short
    else
	echo -e "[${RED}!${RESET}] No DMARC records found"
    fi

    # Check if the HEADER_FILE is provided
    if [[ ! -z $HEADER_FILE ]]
    then
	echo -e "[${YELLOW}+${RESET}] Checking for DKIM"

	# Check if the DKIM_SELECTORS exist
	if [[ ! -z $DKIM_SELECTORS ]]
	then
	    # Check all the selectors
	    for i in ${DKIM_SELECTORS[@]}
	    do
		dig txt $i._domainkey.$DOMAIN +short
	    done
	else
	    echo -e "[${RED}!${RESET}] No DKIM selectors found"
	fi
    fi
else
    echo -e "[${RED}!${RESET}] No MX records found"
fi

