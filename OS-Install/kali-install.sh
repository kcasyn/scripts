#!/bin/bash

# Original script by kcasyn and JxTX

# set colors for use throughout the script.
RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;33m"
BLUE="\033[01;34m"
BOLD="\033[01;01m"
RESET="\033[00m"
export DEBIAN_FRONTEND=noninteractive

# check if running on kali, if not, exit.
if (lsb_release -d | grep -q "Kali" && lsb_release -c | grep -q "kali-rolling")
then
    echo ""
else
    echo -e "\n[${RED}!${RESET}] this is ${RED}not${RESET} Kali"
    exit
fi

# set global install directory
if [[ ! -d $HOME/Tools ]]
then
    mkdir $HOME/Tools
fi
installdir=$HOME/Tools

# perform updates so we aren't doing it in every function
echo -e "\n[${GREEN}+${RESET}] performing ${YELLOW}updates${RESET}"
apt-get -qq update && \
    apt-get -qq upgrade -y

# perform install of common tools so we aren't doing it in (almost) every function
echo -e "\n[${GREEN}+${RESET}] installing common ${YELLOW}system tools${RESET} and ${YELLOW}pipenv${RESET}"
apt-get -qq install -y build-essential curl git nano nmap netcat-traditional wget whois wireshark httpie \
	python python3 python-dev python3-dev python3-pip \
	postgresql \
	tmux \
	firefox-esr \
	exploitdb
pip3 install pipenv

########################
# System Configuration #
########################

# basic tmux configuration improvements
echo -e "\n[${GREEN}+${RESET}] configuring ${YELLOW}tmux${RESET}"

# let's do this for all users
FILE=$HOME/.tmux.conf
if [ ! -f "$FILE" ];
then
    cat <<EOF > $HOME/.tmux.conf
set -g default-terminal "screen-256color" # colors!
setw -g xterm-keys on
set -s escape-time 10                     # faster command sequences
set -sg repeat-time 600                   # increase repeat timeout
set -s focus-events on

set -g prefix2 C-a                        # GNU-Screen compatible prefix
bind C-a send-prefix -2

set -q -g status-utf8 on                  # expect UTF-8 (tmux < 2.2)
setw -q -g utf8 on

set -g history-limit 500000                # boost history

# -- display -------------------------------------------------------------------

set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows

setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed

set -g set-titles on          # set terminal title

set -g display-panes-time 800 # slightly longer pane indicators display time
set -g display-time 1000      # slightly longer status messages display time

set -g status-interval 1      # redraw status line every 10 seconds

# clear both screen and history
bind -n C-l send-keys C-l \; run 'sleep 0.1' \; clear-history

# activity
set -g monitor-activity on
set -g visual-activity off

# -- navigation ----------------------------------------------------------------

# create session
bind C-c new-session

# find session
bind C-f command-prompt -p find-session 'switch-client -t %%'

# split current window horizontally
bind - split-window -v
# split current window vertically
bind _ split-window -h

# pane navigation
bind -r h select-pane -L  # move left
bind -r j select-pane -D  # move down
bind -r k select-pane -U  # move up
bind -r l select-pane -R  # move right
bind > swap-pane -D       # swap current pane with the next one
bind < swap-pane -U       # swap current pane with the previous one

# maximize current pane
bind + run 'cut -c3- ~/.tmux.conf | sh -s _maximize_pane "#{session_name}" #D'

# pane resizing
bind -r H resize-pane -L 2
bind -r J resize-pane -D 2
bind -r K resize-pane -U 2
bind -r L resize-pane -R 2

# window navigation
unbind n
unbind p
bind -r C-h previous-window # select previous window
bind -r C-l next-window     # select next window
bind Tab last-window        # move to last active window

# reload configuration
bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'

# -- windows & pane creation ---------------------------------------------------

# new window retains current path, possible values are:
#   - true
#   - false (default)
tmux_conf_new_window_retain_current_path=false

# new pane retains current path, possible values are:
#   - true (default)
#   - false
tmux_conf_new_pane_retain_current_path=true

# new pane tries to reconnect ssh sessions (experimental), possible values are:
#   - true
#   - false (default)
tmux_conf_new_pane_reconnect_ssh=false

# prompt for session name when creating a new session, possible values are:
#   - true
#   - false (default)
tmux_conf_new_session_prompt=false


# -- display -------------------------------------------------------------------

# RGB 24-bit colour support (tmux >= 2.2), possible values are:
#  - true
#  - false (default)
tmux_conf_theme_24b_colour=true

# window style
tmux_conf_theme_window_fg='default'
tmux_conf_theme_window_bg='default'

# highlight focused pane (tmux >= 2.1), possible values are:
#   - true
#   - false (default)
tmux_conf_theme_highlight_focused_pane=false

# focused pane colours:
tmux_conf_theme_focused_pane_fg='default'
tmux_conf_theme_focused_pane_bg='#0087d7'               # light blue

# pane border style, possible values are:
#   - thin (default)
#   - fat
tmux_conf_theme_pane_border_style=thin

# pane borders colours:
tmux_conf_theme_pane_border='#444444'                   # gray
tmux_conf_theme_pane_active_border='#00afff'            # light blue

# pane indicator colours
tmux_conf_theme_pane_indicator='#00afff'                # light blue
tmux_conf_theme_pane_active_indicator='#00afff'         # light blue

# status line style
tmux_conf_theme_message_fg='#000000'                    # black
tmux_conf_theme_message_bg='#f45a07'                    # orange # yellow #ffff00
tmux_conf_theme_message_attr='bold'

# status line command style (<prefix> : Escape)
tmux_conf_theme_message_command_fg='#ffff00'            # yellow
tmux_conf_theme_message_command_bg='#000000'            # black
tmux_conf_theme_message_command_attr='bold'

# window modes style
tmux_conf_theme_mode_fg='#000000'                       # black
tmux_conf_theme_mode_bg='#ffff00'                       # yellow
tmux_conf_theme_mode_attr='bold'

# status line style
tmux_conf_theme_status_fg='#8a8a8a'                     # light gray
tmux_conf_theme_status_bg='#080808'                     # dark gray
tmux_conf_theme_status_attr='none'

# terminal title
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_terminal_title='#h #S ● #I #W'

# window status style
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_window_status_fg='#8a8a8a'              # light gray
tmux_conf_theme_window_status_bg='#080808'              # dark gray
tmux_conf_theme_window_status_attr='none'
tmux_conf_theme_window_status_format='#I #W'
#tmux_conf_theme_window_status_format='#{circled_window_index} #W'
#tmux_conf_theme_window_status_format='#I #W#{?window_bell_flag,🔔,}#{?window_zoomed_flag,🔍,}'

# window current status style
#   - built-in variables are:
#     - #{circled_window_index}
#     - #{circled_session_name}
#     - #{hostname}
#     - #{hostname_ssh}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_window_status_current_fg='#000000'      # black
tmux_conf_theme_window_status_current_bg='#36b9ff'      # light blue
tmux_conf_theme_window_status_current_attr='bold'
tmux_conf_theme_window_status_current_format='#I #W'
#tmux_conf_theme_window_status_current_format='#{circled_window_index} #W'
#tmux_conf_theme_window_status_current_format='#I #W#{?window_zoomed_flag,🔍,}'

# window activity status style
tmux_conf_theme_window_status_activity_fg='default'
tmux_conf_theme_window_status_activity_bg='default'
tmux_conf_theme_window_status_activity_attr='underscore'

# window bell status style
tmux_conf_theme_window_status_bell_fg='#ffff00'         # yellow
tmux_conf_theme_window_status_bell_bg='default'
tmux_conf_theme_window_status_bell_attr='blink,bold'

# window last status style
tmux_conf_theme_window_status_last_fg='#00afff'         # light blue
tmux_conf_theme_window_status_last_bg='default'
tmux_conf_theme_window_status_last_attr='none'

# status left/right sections separators
tmux_conf_theme_left_separator_main=''  #   /!\ you don't need to install Powerline
tmux_conf_theme_left_separator_sub=''   #   you only need fonts patched with
tmux_conf_theme_right_separator_main='' #   Powerline symbols or the standalone
tmux_conf_theme_right_separator_sub=''  #   PowerlineSymbols.otf font

# status left/right content:
#   - separate main sections with '|'
#   - separate subsections with ','
#   - built-in variables are:
#     - #{battery_bar}
#     - #{battery_hbar}
#     - #{battery_percentage}
#     - #{battery_status}
#     - #{battery_vbar}
#     - #{circled_session_name}
#     - #{hostname_ssh}
#     - #{hostname}
#     - #{loadavg}
#     - #{pairing}
#     - #{prefix}
#     - #{root}
#     - #{synchronized}
#     - #{uptime_d}
#     - #{uptime_h}
#     - #{uptime_m}
#     - #{uptime_s}
#     - #{username}
#     - #{username_ssh}
tmux_conf_theme_status_left=' #S '
tmux_conf_theme_status_right='#{prefix}#{pairing}#{synchronized} %l:%M:%S %p , %a %d %b | #{username}#{root} | #{hostname} '

# status left style
tmux_conf_theme_status_left_fg='#000000,#e4e4e4,#e4e4e4'  # black, white , white
tmux_conf_theme_status_left_bg='#ffff00,#00afff'  # yellow, white blue
tmux_conf_theme_status_left_attr='bold,none,none'

# status right style
tmux_conf_theme_status_right_fg='#8a8a8a,#e4e4e4,#000000' # light gray, white, black
tmux_conf_theme_status_right_bg='#080808,#d70000,#e4e4e4' # dark gray, red, white
tmux_conf_theme_status_right_attr='none,none,bold'

# pairing indicator
tmux_conf_theme_pairing='👓 '          # U+1F453
tmux_conf_theme_pairing_fg='none'
tmux_conf_theme_pairing_bg='none'
tmux_conf_theme_pairing_attr='none'

# prefix indicator
tmux_conf_theme_prefix='⌨ '            # U+2328
tmux_conf_theme_prefix_fg='none'
tmux_conf_theme_prefix_bg='none'
tmux_conf_theme_prefix_attr='none'

# root indicator
tmux_conf_theme_root='!'
tmux_conf_theme_root_fg='none'
tmux_conf_theme_root_bg='none'
tmux_conf_theme_root_attr='bold,blink'

# synchronized indicator
tmux_conf_theme_synchronized='🔒'     # U+1F512
tmux_conf_theme_synchronized_fg='none'
tmux_conf_theme_synchronized_bg='none'
tmux_conf_theme_synchronized_attr='none'

# clock style (when you hit <prefix> + t)
# you may want to use %I:%M %p in place of %R in tmux_conf_theme_status_right
tmux_conf_theme_clock_colour='#00afff'  # light blue
tmux_conf_theme_clock_style='12'

# -- clipboard -----------------------------------------------------------------

# in copy mode, copying selection also copies to the OS clipboard
#   - true
#   - false (default)
# on macOS, this requires installing reattach-to-user-namespace, see README.md
# on Linux, this requires xsel or xclip
tmux_conf_copy_to_os_clipboard=false
EOF
fi

# enable ssh access
# creates a backup of the original config, then creates a more secure version based on
# https://infosec.mozilla.org/guidelines/openssh for modern SSH Server Configuration
enableSSH(){
    echo -e "\n[${GREEN}+${RESET}] changing SSH to ${RED}public key only${RESET}, enabling ssh access and configuring sshd_config"

    echo -e "\n[${GREEN}+${RESET}] make sure ssh server is installed"
    # make sure ssh server is installed
    apt-get -qq install -y openssh-server

    echo -e "\n[${GREEN}+${RESET}] backup ssh config files"
    # make backups of original config files
    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
    cp /etc/ssh/ssh_config /etc/ssh/ssh_config.bak
    cp /etc/ssh/moduli /etc/ssh/moduli.bak

    echo -e "\n[${GREEN}+${RESET}] modify moduli file"
    # edit moduli file
    awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.tmp && \
        mv /etc/ssh/moduli.tmp /etc/ssh/moduli

    echo -e "\n[${GREEN}+${RESET}] remove default host ssh keys"
    # remove default host keys
    rm -f /etc/ssh/ssh_host_dsa_key*
    rm -f /etc/ssh/ssh_host_rsa_key*
    rm -f /etc/ssh/ssh_host_ed25519_key*

    echo -e "\n[${GREEN}+${RESET}] generate new ed25519 host key"
    # generate new host key
    ssh-keygen -t ed25519 -N "" -f /etc/ssh/ssh_host_ed25519_key

    echo -e "\n[${GREEN}+${RESET}] generate new sshd_config file"
    # generate new sshd_config
    cat <<EOF > /etc/ssh/sshd_config
# Based on OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp and https://infosec.mozilla.org/guidelines/openssh for modern SSH Server Configuration

## Secure configuration changes

# Host key and Algorithms
HostKey /etc/ssh/ssh_host_ed25519_key
KexAlgorithms curve25519-sha256@libssh.org
Ciphers chacha20-poly1305@openssh.com
MACs hmac-sha2-512-etm@openssh.com

# Logging
LogLevel VERBOSE
SyslogFacility AUTHPRIV
Subsystem sftp /usr/lib/openssh/sftp-server -f AUTHPRIV -l INFO

# Authentication
UsePAM yes
PermitRootLogin no
PubkeyAuthentication yes
PasswordAuthentication no
AuthenticationMethods publickey
ChallengeResponseAuthentication no
AuthorizedKeysFile .ssh/authorized_keys

# Sandboxing
UsePrivilegeSeparation sandbox

# Misc options
PrintMotd no
X11Forwarding no

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

## https://infosec.mozilla.org/guidelines/openssh Modern(OpenSSH 6.7+) recommended

# Supported HostKey algorithms by order of preference.
#HostKey /etc/ssh/ssh_host_ed25519_key
#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key

#KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256

#Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

#MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com

# Password based logins are disabled - only public key based logins are allowed.
#AuthenticationMethods publickey

# LogLevel VERBOSE logs user's key fingerprint on login. Needed to have a clear audit track of which key was using to log in.
#LogLevel VERBOSE

# Log sftp level file access (read/write/etc.) that would not be easily logged otherwise.
#Subsystem sftp  /usr/lib/ssh/sftp-server -f AUTHPRIV -l INFO

# Root login is not allowed for auditing reasons. This is because it's difficult to track which process belongs to which root user:
#
# On Linux, user sessions are tracking using a kernel-side session id, however, this session id is not recorded by OpenSSH.
# Additionally, only tools such as systemd and auditd record the process session id.
# On other OSes, the user session id is not necessarily recorded at all kernel-side.
# Using regular users in combination with /bin/su or /usr/bin/sudo ensure a clear audit track.
#PermitRootLogin No

# Use kernel sandbox mechanisms where possible in unprivileged processes
# Systrace on OpenBSD, Seccomp on Linux, seatbelt on MacOSX/Darwin, rlimit elsewhere.
#UsePrivilegeSeparation sandbox

## Default OpenSSH sshd_config can be found at /etc/ssh/sshd_config.bak
EOF

    echo -e "\n[${GREEN}+${RESET}] generate new ssh_config file"
    # generate new ssh_config
    cat <<EOF > /etc/ssh/ssh_config
## ssh_config based on https://infosec.mozilla.org/guidelines/openssh OpenSSH Client Modern
# Ensure KnownHosts are unreadable if leaked - it is otherwise easier to know which hosts your keys have access to.
HashKnownHosts yes

# Host keys the client accepts - order here is honored by OpenSSH
HostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-rsa,ecdsa-sha2-nistp521-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp384,ecdsa-sha2-nistp256

# Key Exchange Algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group18-sha512,diffie-hellman-group16-sha512,diffie-hellman-group14-sha256,diffie-hellman-group-exchange-sha256

# MAC Algorithms
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-md5

# Cipher Algorithms
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

# Enable shared connections
ControlMaster auto
ControlPath /tmp/ssh_mux_%h_%p_%r

# Environment
SendEnv LANG LC_*
GSSAPIAuthentication yes
EOF

    echo -e "\n[${GREEN}+${RESET}] enable ssh server at startup and restart ssh"
    systemctl enable ssh && \
	systemctl restart ssh

    echo -e "\n[${YELLOW}!${RESET}] SSH is now ${RED}public key only with root login disabled${RESET}. Remember to add the ${YELLOW}authorized_keys${RESET} file under /home/$(ls /home)/.ssh"
}

# install ZSH because we like pretty colors and expanded functionality
# we'll use oh-my-zsh since it has a lot of nice functionality
installZSH(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}zsh${RESET}"
    
    # install zsh
    apt-get -qq install -y zsh

    echo -e "\n[${GREEN}+${RESET}] configuring ${YELLOW}zsh${RESET}"	

    # change to home directory
    # download and install oh-my-zsh for fanciness
    # download and install spaceship-prompt theme and zsh-autosuggetionscon
    # configure everything
    cd $HOME && \
	sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" && \
	git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" && \
	ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme" && \
	git clone https://github.com/zsh-users/zsh-autosuggestions "$ZSH_CUSTOM/plugins/zsh-autosuggestions" && \
	sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="spaceship"/' .zshrc && \
	sed -i 's/plugins=(git)/plugins=(git debian web-search zsh-autosuggestions)/' .zshrc && \
	cat <<EOF >> .zshrc
## Settings
RPROMPT="[%@ ]"
export TMUX=
unset TMPDIR
EOF

    # make the prompt one line
    cd $HOME/.oh-my-zsh/custom/themes/ && \
	sed -i 's/SPACESHIP_PROMPT_SEPARATE_LINE="${SPACESHIP_PROMPT_SEPARATE_LINE=true}"/SPACESHIP_PROMPT_SEPARATE_LINE="${SPACESHIP_PROMPT_SEPARATE_LINE=false}"/'

    # change shell from bash to zsh (requires a relog to take effect)
    chsh -s $(which zsh)
}

######################
# Exploitation Tools #
######################

# install PowerSploit. This is a collection of useful pentest powershell modules.
powerSploit(){
    echo -e "\n[${GREEN}+${RESET}] downloading ${YELLOW}powersploit${RESET}"
    cd $installdir && \
	git clone https://github.com/PowerShellMafia/PowerSploit.git
}

# installs CrackMapExecute. Useful post-exploitaion and situational awareness tool.
cmeInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}CrackMapExec${RESET}"
    apt-get -qq install -y crackmapexec
}

# installs Metasploit Framework and sets up the datbase. Standard pentesting framework.
metasploitInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Metasploit${RESET}"
    apt-get -qq install -y metasploit-framework
}

# Ridenum
installRIDenum(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}RIDenum${RESET}"
    apt-get -qq install -y ridenum
}

# Powershell Empire
installEmpire(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Empire${RESET}"
    apt-get -qq install -y powershell-empire
}

# Impacket
installImpacket(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Impacket Scripts${RESET}"
    apt-get -qq install -y impacket-scripts
}

# Responder
installResponder(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Responder${RESET}"
    apt-get -qq install -y responder
}

# installs Covenant C2 Framework
installCovenant(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Covenant${RESET}"
    cd $installdir && \
	apt-get -qq install -y \
		apt-transport-https ca-certificates gnupg2 software-properties-common docker.io && \
	git clone --recurse-submodules https://github.com/cobbr/Covenant && \
	cd Covenant/Covenant && \
	docker build -t covenant . && \
	echo "covenant(){docker run -it -p 7443:7443 -p 80:80 -p 443:443 --name covenant -v /opt/Covenant/Covenant/Data:/app/Data covenant --username foresite --computername 0.0.0.0}" | tee -a $HOME/.zshrc && \
	echo -e "\n[${GREEN}+${RESET}] Usage:\nIn terminal: ${YELLOW}covenant${RESET}\nFollow prompts."
}

# installs lsassy
installLsassy(){
    pip3 install lsassy
}

# installs Veil-Evasion for payload obfuscation
installVeil(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Veil${RESET}"
    apt-get -qq install veil
}

####################
# Validation Tools #
####################

# installs ssh-audit. Great tool for ssh validation.
sshAuditInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}ssh-audit${RESET}"
    apt-get -qq install -y ssh-audit
}

# installs rdp-sec-check. Great for validating RDP.
rdpSecCheckInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}rdp-sec-check${RESET}"
    cd $installdir && \
	apt-get -qq install -y perl && \
	echo "yes" | cpan install CPAN && \
	cpan install Encoding::BER && \
	git clone https://github.com/portcullislabs/rdp-sec-check.git
}

# installs testssl.sh. Useful for validating SSL/TLS.
testsslInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}testssl.sh${RESET}"
    apt-get -qq install -y testssl.sh
}

# installs onesixtyone. Useful for SNMP validation.
onesixtyoneInstall(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}onesixtyone${RESET}"
    apt-get -qq install -y onesixtyone
}

##############
# Misc Tools #
##############

# installs Remmina RDP client.
installRemmina(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}remmina${RESET}"
    apt-get -qq install -y remmina
}

# installs NFS tools for mounting network file shares.
installNFStools(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}nfs-common${RESET} and ${YELLOW}cifs-utils${RESET}"
    apt-get -qq install -y nfs-common cifs-utils
}

# installs an FTP client
installFTPStuff(){
    echo -e "\n[${GREEN}+${RESET}] installing an ${YELLOW}ftp${RESET} client"
    apt-get -qq install -y ftp
}

# installs burpsuite community
installBurpsuite(){
    echo -e "\n[${GREEN}+${RESET}] installing ${YELLOW}Burpsuite${RESET}"
    apt-get install -y burpsuite
}

# display the menu and wait for a choice.
menu(){
    echo -e \
"Kali Installation Script\n\n\
What you want to do?\n\n\
	0 Enable SSH\n\
	1 Install zprezto\n\
	2 Install CrackMapExec\n\
	3 Install PowerSploit\n\
	4 Install Metasploit Framework\n\
	5 Install ssh-audit\n\
	6 Install rdp-sec-check\n\
	7 Install testssl.sh\n\
	8 Install onesixtyone\n\
	9 Install Terminator\n\
	10 Install Remmina\n\
	11 Install nfs-tools\n\
	12 Install FTP client\n\
	13 Install RID enum\n\
	14 Install Empire\n\
	15 Install Impacket\n\
	16 Install Responder\n\
	17 Install Covenant\n\
	18 Install lsassy\n\
	19 Install Veil\n\
	20 Install Burpsuite\n\
	21 Install All\n\
	22 Exit\n"
}

if [ "$1" = "full" ]
   then
       enableSSH
       installZSH
       cmeInstall
       powerSploit
       metasploitInstall
       sshAuditInstall
       rdpSecCheckInstall
       testsslInstall
       onesixtyoneInstall
       terminatorInstall
       installNFStools
       installFTPStuff
       installRemmina
       installRIDenum
       installEmpire
       installImpacket
       installResponder
       installCovenant
       installLsassy
       installVeil
       installBurpsuite
else
    while : ;
    do
	menu
	read -rp "Enter choice: " choice
	case $choice in
	    0)enableSSH;;
	    1)installZSH;;
	    2)cmeInstall;;
	    3)powerSploit;;
	    4)metasploitInstall;;
	    5)sshAuditInstall;;
	    6)rdpSecCheckInstall;;
	    7)testsslInstall;;
	    8)onesixtyoneInstall;;
	    9)terminatorInstall;;
	    10)installRemmina;;
	    11)installNFStools;;
	    12)installFTPStuff;;
	    13)installRIDenum;;
	    14)installEmpire;;
	    15)installImpacket;;
	    16)installResponder;;
	    17)installCovenant;;
	    18)installLsassy;;
	    19)installVeil;;
	    20)installBurpsuite;;
	    21)enableSSH && installZSH && cmeInstall && powerSploit && metasploitInstall && sshAuditInstall && rdpSecCheckInstall && testsslInstall && sslscanInstall && onesixtyoneInstall && terminatorInstall && installNfstools && installFTPStuff && installRemmina && installRIDenum && installEmpire && installImpacket && installResponder && installLsassy && installVeil && installBurpsuite;;
	    22)clear && break ;;
	    *) echo -e "\n[${RED}!${RESET}]Choice not in list" >&2; exit 2
	esac
    done
fi
