#!/usr/bin/python3
# Adapted by kcasyn from: https://gist.github.com/rxwx/d07495f790d62029b12065c38ac2a86a

import requests
import sys
import re
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

HEADERS = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:67.0) Gecko/20100101 Firefox/67.0"}

if len(sys.argv) != 2:
    print (" Usage: python pulseversion.py <target ip/domain>")
    sys.exit(1)

print ("[+] Trying nc_gina_ver.txt for version info...")
r = requests.get("https://%s/dana-na/nc/nc_gina_ver.txt" % sys.argv[1], verify=False, allow_redirects=False)

if r.status_code == 200:
    reg = re.compile(r'<PARAM NAME="ProductVersion" VALUE="([\d.]*?)"')
    result = reg.search(r.text)
    if result:
        print ("[+] %s, version: %s" % (sys.argv[1], result.group(1)))
    else:
        print ("[!] Unable to detect version")
else:    
    print ("[!] Couldn't find target file")

print ("[+] Trying HostCheckerInstaller.osx for version info...")
r = requests.get("https://%s/dana-cached/hc/HostCheckerInstaller.osx" % sys.argv[1], verify=False, allow_redirects=False)

if r.status_code != 200:
    print ("[!] Couldn't find Host Checker")
    sys.exit(1)

pattern = re.compile("<key>version</key>[^<]*<string>([^>]+)<")
result = pattern.findall(str(r.content))

if len(result) == 1:
    print ("[+] %s, version: %s" % (sys.argv[1], result[0]))
else:
    print ("[!] Unable to detect version")
