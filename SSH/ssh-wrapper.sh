#!/bin/bash

# Script location: /usr/local/bin/ssh-wrapper.sh
# authorized_keys usage:
# command="ssh-wrapper.sh",restrict,pty ssh-ed25519 $pubkey $comment

# This script is used on a jump host to add new ssh keys that are only allowed to do portforwarding
# for creating reverse ssh connections.

# regex to check the incoming SSH command
# assumes an ssh-ed25519 key
# checks for the 'echo' command
# checks for the authorized_keys file options 'command="exit",restrict,port-forwarding'
# checks for an ssh-ed25519 key with a comment format of $portnum_$hostname_$day-$monthabbrev-$year_$hour:$minute
REGEX="^echo command=\"exit\",restrict,port-forwarding ssh-ed25519 [a-zA-Z0-9\+\/]{68} [0-9]{5}_[0-9a-zA-Z\_\-]{1,15}_[0-9][0-9]-[ADFJMNOS][aceopu][bcglnprtvy]-[0-9][0-9][0-9][0-9]_[0-9][0-9]:[0-9][0-9]$"

# check the original command against the regex
if [[ $SSH_ORIGINAL_COMMAND =~ $REGEX ]]
then
    # the echo command matches the regex, add the key and options to the authorized_keys file
    $SSH_ORIGINAL_COMMAND | tee -a ~/.ssh/authorized_keys
fi
